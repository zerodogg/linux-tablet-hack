# linux-tablet-hack

This is a somewhat hacky daemon that lets you execute commands when a laptop
switches from being used as a laptop to being used as a tablet, for multi
form-factor devices. Preferably you should use
[linuxflip](https://github.com/yehuthi/linuxflip) or
[linux_detect_tablet_mode](https://github.com/alesya-h/linux_detect_tablet_mode),
but those only work on devices that expose mode switch events through libinput,
which some devices (like my Asus Transformer TP300LA) doesn't.

The way this variant implements it is by reading events from `monitor-sensor`
from `iio-sensor-proxy`, assuming "tablet mode" whenever the rotation is not
"normal" (ie. when you're holding the device on its side, or up-side-down).
This isn't *really* detection of tablet mode, but works well enough as a
workaround.

## Installation

1. Clone the git repository
2. Install the dependencies listed below
3. Perform configuration as listed below
4. Run `./linux-tablet-hack` to test that it works as you want
5. Optionally run `./linux-tablet-hack --install-autostart` to configure
   linux-tablet-hack to autostart on login

## Dependencies

`linux-tablet-hack` needs the perl module `YAML`. On most RPM-based
distributions it is packaged as `perl-YAML`, on most Debian-based distributions
it is packaged as `libyaml-perl`. It also needs the command `monitor-sensor`
from `iio-sensor-proxy`.

On openSUSE it can be installed with: `sudo zypper install -y perl-YAML
iio-sensor-proxy`  
On Debian/Ubuntu/.. it can be installed with: `sudo apt install -y libyaml-perl
iio-sensor-proxy`

## Configuration

The configuration syntax is derived from the syntax used by
[linux_detect_tablet_mode](https://github.com/alesya-h/linux_detect_tablet_mode),
for consistency. You specify certain commands to be executed whenever the
"mode" changes, and `linux-tablet-hack` will do just that.

Usually what you will want to do is enable and disable the keyboard and
touchpad. You can use the `xinput` command to do this (as long as you're using
X.org and not Wayland). Run `xinput` (without any parameters) to list available
devices, and specify the ones you want to enable/disable in your configuration
like in the example below.

If you change the configuration file while `linux-tablet-hack` is running, the
changes will be picked up the next time the mode changes.

Example:

```yaml
modes:
  laptop:
    - xinput enable 'ETPS/2 Elantech Touchpad'
    - xinput enable 'AT Translated Set 2 keyboard'
  tablet:
    - xinput disable 'ETPS/2 Elantech Touchpad'
    - xinput disable 'AT Translated Set 2 keyboard'
```

## License

linux-tablet-hack is Copyright © Eskild Hustvedt 2024

linux-tablet-hack is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

linux-tablet-hack is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program. If not, see https://www.gnu.org/licenses/gpl-3.0.txt.
