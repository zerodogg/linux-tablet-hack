#!/usr/bin/perl
# linux-tablet-hack
# Copyright (C) Eskild Hustvedt 2024
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
use 5.030;
use feature 'signatures';
no warnings 'experimental::signatures';
use YAML qw(LoadFile);
use File::Path qw(mkpath);
use File::Basename qw(basename);
use Cwd qw(realpath);
use Getopt::Long;

my $verbosity = 0;

# Reads a file into a scalar and returns it
sub slurp($file)
{
    open(my $in,'<',$file) or die("Failed to open $file for reading: $!");
    local $/ = undef;
    my $content = <$in>;
    close($in);
    return $content;
}

# A *very simple* lockfile implementation
# This is very naive, and doesn't use any locking primitives from the OS, so there
# is a theoretical chance for a race condition, but for our purposes this is more than
# sufficient.
#
# It does not remove the lockfile on exit, but will verify that the PID actually belongs
# to linux-tablet-hack before deciding that we're locked.
sub lockOrDie ()
{
    # We use /dev/shm to store the file, so it needs to be writeable
    if (!-w '/dev/shm')
    {
        die("/dev/shm: is not writeable. Unable to write lockfile. Use the --no-lock parameter to\ndisable use of a lockfile.\n");
    }
    # The path to the lockfile. User-specific filename since /dev/shm could be shared.
    my $locked = '/dev/shm/.linux-tablet-hack-'.$<.'-'.$>.'.lock';
    # If we have a lockfile
    if (-e $locked)
    {
        # Fetch the PID from it
        my $PID = slurp($locked);
        sayd("Lockfile existed, containing $PID");
        # If the PID looks like a real PID and is a process we can read info about
        if ($PID =~ /^\d+$/ && -d '/proc/'.$PID && -r '/proc/'.$PID.'/cmdline' && $PID ne $$)
        {
            # Read the cmdline
            my $cmdline = slurp('/proc/'.$PID.'/cmdline');
            # Find out our own name
            my $appname = basename($0);
            # Check for our name in the cmdline of the process with the PID from the lockfile
            if(index($cmdline,$appname) != -1)
            {
                # If all matches, then it looks like a copy of the helper is already running
                if ($verbosity > -1)
                {
                    warn("linux-tablet-hack appears to already be running (PID $PID). Refusing to run a second instance.\n".
                        "If you are sure it's not running, or want to force-start a second instance,\n".
                        "either use the --no-lock parameter or remove the $locked file\n");
                }
                exit(5);
            }
        }
        sayd("Lockfile is no longer valid - ignoring it");
    }
    open(my $out,'>',$locked);
    print {$out} $$;
    close($out);
    sayd("Wrote lockfile to $locked");
}

# Wrapper around 'say' that appends the program name to it
sub sayw($message)
{
    say 'linux-tablet-hack: '.$message;
}

# Outputs a message unless we're in quiet mode
sub saym($message)
{
    if ($verbosity > -1)
    {
        sayw($message);
    }
}

# Outputs a message in verbose mode
sub sayv($message)
{
    if ($verbosity > 0) {
        sayw($message);
    }
}

# Outputs a debugging message
sub sayd($message)
{
    if ($verbosity >= 100) {
        sayw($message);
    }
}

# Retrieves the path to the configuration file
sub getConfigFilePath ()
{
    # At some point this could probably be smarter
    return 'linux-tablet-hack.yml';
}

# Purpose: Check for a file in path
# Usage: InPath(FILE)
sub InPath
{
	foreach (split /:/, $ENV{PATH}) { if (-x "$_/@_" and not -d "$_/@_" ) {	return 1; } } return 0;
}

# Retrieves the contents of the configuration file, re-reading the file if it
# has changed since the last time it was read
sub getCommands ()
{
    state $configPath = getConfigFilePath();
    state $modified = -1;
    state $config;

    my $currModifiedTime = (stat($configPath))[9];

    if ($currModifiedTime ne $modified)
    {
        $modified = $currModifiedTime;
        $config = LoadFile($configPath);
    }
    return $config;
}

# Runs mode change commands for the specified mode
sub runModeChangeCommands($mode)
{
    my $config = getCommands();
    foreach my $command (@{$config->{modes}->{$mode}})
    {
        system($command);
    }
}

# Emit a mode change event, and if it differs from the previous mode then
# runModeChangeCommands
sub modeChange($mode)
{
    state $prevMode = 'laptop';
    if ($mode ne $prevMode)
    {
        sayd("Mode changed from $prevMode to $mode");
        runModeChangeCommands($mode);
        $prevMode = $mode;
    }
}

# Our main loop. Reads from monitor-sensor to detect accelerometer changes.
sub monitorLoop ()
{
    # Make sure monitor-sensor output is never localized
    $ENV{LC_ALL} = 'C';
    while(1)
    {
        open(my $in,'-|',qw(monitor-sensor --accel)) or die("Failed to open communication with monitor-sensor: $!");
        while(my $line = <$in>)
        {
            if ($line =~ /Accelerometer orientation changed/)
            {
                if ($line =~ /:\s*normal/)
                {
                    modeChange('laptop');
                }
                else
                {
                    modeChange('tablet');
                }
            }
        }
        close($in);
        sayw("monitor-sensor exited for some reason, restarting...");
    }
}

# Output help
sub help ()
{
    say "Usage: $0 [options]";
    say "";
    say "Options:";
    say "  -h,  --help              Display this help screen and exit",
    say "       --install-autostart Set linux-tablet-hack to autostart";
    say "                           on login";
    say "       --no-lock           Disable use of a lockfile";
    # Currently this doesn't actually do anything
    #say "  -v,  --verbose           Increase verbosity";
    say "       --debug             Enable debugging messages";
}

# Install our autostart file
sub installAutostart ()
{
    my $XDG_CONFIG_HOME = $ENV{XDG_CONFIG_HOME} ? $ENV{XDG_CONFIG_HOME} : $ENV{HOME}.'/.config';
    mkpath($XDG_CONFIG_HOME.'/autostart');

    open(my $out,'>',$XDG_CONFIG_HOME.'/autostart/linux-tablet-hack.desktop');
    print {$out} '[Desktop Entry]'."\nType=Application\nName=linux-tablet-hack\nHidden=false\nX-GNOME-Autostart-enabled=true\n";
    print {$out} 'Exec='.realpath($0).' --autostarted'."\n";
    close($out);
    chmod(0755,$XDG_CONFIG_HOME.'/autostart/linux-tablet-hack.desktop');

    say "Installed autostart file to ".$XDG_CONFIG_HOME.'/autostart/linux-tablet-hack.desktop';
}

# Main entry point
sub main ()
{
    my $noLock = 0;
    GetOptions(
        'no-lock' => \$noLock,
        'verbose|v' => sub { $verbosity++; },
        'debug' => sub { $verbosity = 100; },
        'quiet|q' => sub { $verbosity = -1; },
        'h|help' => sub {
            help();exit(0);
        },
        # No-op for now
        'autostarted' => sub {},
        'install-autostart' => sub {
            installAutostart();
            exit(0);
        }
    ) or die("See $0 --help\n");

    if (!InPath('monitor-sensor'))
    {
        die("linux-tablet-hack requires the 'monitor-sensor' program to work.\nPlease install this and then re-run linux-tablet-hack.\nIt is a part of 'iio-sensor-proxy' (see the README.md for details).\n");
    }

    if (!$noLock)
    {
        lockOrDie();
    }

    monitorLoop();
}

main();
